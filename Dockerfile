FROM golang:1.17.3-alpine3.14 as build

WORKDIR /home/workspace
COPY ./ workspace

RUN go build main.go

FROM golang:1.17.3-alpine3.14

COPY --from=build /home/workspace/main /usr/local/bin/main

CMD ["/usr/local/bin/main"]